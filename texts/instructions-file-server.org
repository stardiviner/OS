# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: ../config.org

#+TITLE: Battle of the Threads
#+AUTHOR: Jens Lechtenbörger ([[https://creativecommons.org/licenses/by-sa/4.0/legalcode][CC BY-SA 4.0]])
#+DATE: September 2017

* Introduction
This document describes rules for a variant of the game BattleThreads
suggested in cite:HRB+03.  This game variant aims for the following
learning objective:  Explain race conditions.


* Preparations
** Build Teams
Build teams with 6 players each, number players from 1 to 6.

** Position Ships
As team, fill out the file
[[https://gitlab.com/oer/OS/raw/master/org-resources/our-ships.org][our-ships.org]]
representing your ships in a text editor of your choice.  The sample
file already contains some ideas of what to write down in individual
fields: “X” for an own ship (one ships with three fields is shown in
A1, A2, A3), “h” for a hit, “m” for a miss, “d” for the final hit
destroying a ship.  You may want to move that first ship and erase the
miss in B1.  Position 5 ships (two with 4 fields, three with 3
fields).

** Create Shared Folder
Create a folder in Sciebo (which is an
[[https://owncloud.org/][ownCloud]] instance;
[[https://nextcloud.com/][Nextcloud]] could also be used),
and share that folder among team members (with edit permissions).

** Upload
Upload the file recording your ships to the shared folder.

In the file
[[https://gitlab.com/oer/OS/raw/master/org-resources/their-territory.org][their-territory.org]],
you record your own shots into the opponent’s territory.  That file
contains marks showing a hit at A1 and a miss at B1.  Per team, erase
those, and upload the file to your team’s shared folder.

** Take Positions
Find another team with which to play, one is team A, the other one
team B.  Build 6 pairs of players, each consisting of players from the
two different teams.  Find neighboring seats for each pair such that
pairs are spread throughout the lecture hall (for independent playing
of individual pairs).


* Gameplay
The two players of each pair talk (in low voice) about shots and
outcome.  Team members shoot concurrently.  Before taking a shot, the
shooter downloads the team’s current state of the opponent’s
territory, selects a field, and asks the neighbor.  The neighbor
downloads the state of their own ships, marks whether that is a hit,
miss, or destruction, uploads the file again, and informs the shooter
about the outcome.  The shooter enters the outcome into their file and
uploads it to the server.  Then both neighbors switch roles (the other
one shoots now).

In the first round, actions will be coordinated by external signals,
with shots taken in turn (for this purpose, teams have letters A and
B, and members are numbered).  Afterwards, pairs of neighbors proceed
according to their own pace.


* Acknowledgment
This game variant with files as shared resources and physically
distributed players causing and experiencing race conditions was
proposed by Adrian Lison in response to
[[https://oer.gitlab.io/OS/OS05-MX.html#/slide-mx-feedback][this question]].

bibliographystyle:alphadin
bibliography:literature.bib

import java.util.*;

public class Producer implements Runnable
{
    private BoundedBuffer buffer;
    private String name;

    public Producer(BoundedBuffer b, String n) {
        buffer = b;
        name = n;
    }

    public void run() {
        while (true) {
            int time = (int) (Math.random() * 1000);
            try {
                //Thread.sleep(time);
                System.out.println(name + " before insert(), size == " + buffer.size());
                String item = String.valueOf(time);
                buffer.insert(item);
                System.out.println(name + " entered: " + item);
            }
            catch(InterruptedException e) { }
      }
   }
}

# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: config.org

#+TITLE: OS09: Processes
#+SUBTITLE: Based on Chapter 7 and Section 8.3 of cite:Hai17
#+AUTHOR: Dr. Jens Lechtenbörger
#+DATE: Computer Structures and Operating Systems 2017

* Introduction

# This is the ninth presentation for this course.
#+CALL: generate-plan(number=9)

#+TOC: headlines 1

** Processes
   - First approximation: Process ≈ *program in execution*
     - However
       - Single program can create multiple processes
	 - E.g., web browser with process per tab model
       - What looks like a separate program may not live inside its
         own process
	 - E.g., separate [[https://www.gnu.org/software/emacs/][GNU Emacs]]
           window showing PDF file via [[https://github.com/politza/pdf-tools][PDF Tools]]
	 - (Window contents might be produced with help of different
           process, though)
   #+ATTR_REVEAL: :frag appear
   - Reality: Process = Whatever your OS defines as such
     - Unit of *management* and *protection*
       - One or more threads of execution
       - Address space in virtual memory, shared by threads within process
       - Management information
	 - Access rights
	 - Resource allocation
	 - Miscellaneous context

*** Aside: Single Address Space Systems
    - We only consider the case where each process has its own address space
      - OS acts as *multiple address space system*
      - OS mainstream
    - See cite:Hai17 for some details on *single address space systems*
      - E.g., AS/400

** Process Creation
   - OS starts
     - Check your OS’s tool of choice to inspect processes after boot
   #+ATTR_REVEAL: :frag appear
   - User starts program
     - Touch, click, type
   - Processes start other processes
     - POSIX Process Management API in cite:Hai17

** Process Control Block
   - Recall that [[file:OS03-Threads.org::#tcb][thread control blocks]] record management information
     about threads
   - OS has similar data structure for processes, *process control block*
     - Numerical IDs (e.g., own and parent, executing user)
     - Address space information
     - Privileges
     - Resources (shared by threads)
       - E.g., [[#file-descriptors][file descriptors]]
     - Interprocess communication
       - Flags, signals, messages


* Thread Implementation

** Questions and Alternatives
   - Design questions
     - Can threads be blocked *individually*?
     - How *costly* are context switches?
   #+ATTR_REVEAL: :frag appear
   - Design alternatives
     - Kernel threads
       1. Pure in-kernel system threads ([[#fig-7.8][Fig. 7.8 (a)]])
       2. Threads scheduled by kernel on behalf of applications
	  ([[#fig-7.8][Fig. 7.8 (b)]])
     - User threads ([[#fig-7.8][Fig. 7.8 (c)]])
       - Invisible to kernel
     - (Hybrids)

*** Reminder: Processor Modes and System Calls
    - Recall that CPU cores may run in
      [[file:OS02-Interrupts.org::#kernel-mode][kernel or user mode]]
    - Recall that threads invoke
      [[file:OS02-Interrupts.org::#irq-syscall][system calls]]
      for OS functionality in kernel mode

** Thread Alternatives
    :PROPERTIES:
    :CUSTOM_ID: fig-7.8
    :reveal_extra_attr: data-audio-src="./audio/9-thread-variants.ogg"
    :END:
     #+CAPTION: Figure 7.8 of cite:Hai17
     #+ATTR_HTML: :alt Three thread variants
     #+ATTR_LATEX: :width 0.9\textwidth
     [[./img/hail_f0708.pdf.png]]

     -
       - Relationships between threads,
	 scheduling and dispatching code, and CPU’s operating mode
	 - (a) Threads, scheduler, and dispatcher in kernel mode
	 - (b) Threads run mostly in user mode, scheduled and dispatched
           in kernel
	 - (c) Threads along with a user-level scheduler and dispatcher in
           user mode
#+BEGIN_NOTES
Three possible relationships between threads, code for scheduling and
dispatching, and the CPU’s processor mode are visualized here and
explained on subsequent slides.

In this figure, the vertical curvy lines represent threads, while the
arrows between them indicate whether switches between threads occur in
user mode or kernel mode.  (The different heights of arrows in variants
(a) and (b) has no special meaning.)

Note that on OSs with multithreading support, the usual threads
created by programmers are those of variant (b).  E.g., when you
create a thread in Java on an OS with multithreading support, the JVM
invokes a system call for the creation of a thread by the kernel.
Most of your thread’s code runs in user mode, but occasionally
execution switches to kernel mode for system calls.  Also scheduling
of threads is performed by the OS in kernel mode.

If you want to program with multiple threads on a singlethreaded OS,
you need to resort to variant (c) and manage threads in user mode.

Variant (a) is used by OS developers.
#+END_NOTES

*** Kernel Threads, Variant (a) of Fig. 7.8
    - Kernel manages processes and threads
      #+ATTR_LATEX: :width 0.2\textwidth
      [[file:./img/hail_f0708.a.png]]
      - Scheduling on thread basis
    #+ATTR_REVEAL: :frag appear
    - Figure shows threads that are internal to the kernel
      - Housekeeping, e.g., zeroing memory or writing dirty pages
	to disk
    - Execution, context switching, scheduling all *within* kernel mode
      - Almost no overhead

*** Kernel Threads, Variant (b) of Fig. 7.8
    - Kernel manages processes and threads
      #+ATTR_LATEX: :width 0.2\textwidth
      [[./img/hail_f0708.b.png]]
      - Scheduling on thread basis
    - Threads invoke *system calls* for OS functionality
      #+ATTR_REVEAL: :frag (appear)
      - *Overhead*
	- Mode switch from user to kernel mode
	- Maybe scheduling decision (e.g., blocking system call)
	- Mode switch from kernel to user mode
      - Note: Kernel address space may be (inaccessible) part of user
        address space
	- E.g., 3 GiB for processes, 1 GiB for kernel on IA-32
	  - In this case, switching from thread to kernel and back to same
            thread leaves address space unchanged
	  - Above overhead reduced

*** User Threads, Variant (c) of Fig. 7.8
    - Kernel may or may not support multithreading
      #+ATTR_LATEX: :width 0.2\textwidth
      [[./img/hail_f0708.c.png]]
    - Application performs thread management on its own
      - A thread (managed by the OS) creates and manages its own user threads
	- The thread distributes its time slice among its own user threads
	- User threads are unknown to the kernel
      - E.g., [[https://www.gnu.org/software/pth/][GNU Pth – The GNU Portable Threads]]
	- Library with functions in user mode
	  - Creation, termination of threads
	  - Scheduling

*** User Threads
    - Pro
      - Portability
	- Multithreading, even without kernel support
      - Flexibility
	- E.g., application specific scheduling
      - Speed for context switches
	- Without switch to kernel mode
    #+ATTR_REVEAL: :frag appear
    - Con
      - If one user thread gets blocked, entire managing
        thread gets blocked
      - No real parallelism
	- All user threads belonging to managing thread on same CPU core
	- (No issue on single-core systems)

** JiTT Assignment
   :PROPERTIES:
   :reveal_data_state: jitt no-toc-progress
   :END:
   Suppose that
   - “kernel thread” refers to variant (b) of Fig. 7.8,
   - T_K is a kernel thread within process P,
   - T_U is a kernel thread which manages is own user threads T_{U1},
     …, T_{Un}; T_{Ui} refers to one of them.
   - Mark correct statements in [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=840261][Learnweb]].
     - When T_K invokes a blocking system call, all threads of P are blocked.
     - When T_{Ui} invokes a system call, the OS does not know
       about T_{Ui} but only sees a system call coming from T_U and
       handles it (along with potential state changes for T_U) as usual.
     - When T_{Ui} invokes a blocking system call, T_U is blocked by the
       OS.
     - When T_{Ui} invokes a blocking system call, all user threads
       managed by T_U are blocked by the OS.
     - When T_K is preempted, the other threads of P might continue to run
       on other CPU cores.
     - When T_U is preempted, the threads managed by T_U might continue to
       run on other CPU cores.

* File Descriptors
  (See Section 8.3 in cite:Hai17)

** File Descriptors
   :PROPERTIES:
   :CUSTOM_ID: file-descriptors
   :END:
   #+ATTR_REVEAL: :frag (appear)
  - OS represents open files via *integer numbers* called *file descriptors*
    - Files are abstracted as *streams of bytes*
      - Typical operations: Open, close, read, write
  - POSIX standard describes three descriptors for every process
    0. [@0] Standard input, ~stdin~ (e.g., keyboard input)
    1. Standard output, ~stdout~ (e.g., print to screen/terminal)
    2. Standard error, ~stderr~ (e.g., print error message to terminal)
  - Streams can be used for inter-process communication

** Streams for Inter-Process Communication
   - Streams can be *redirected*
     - E.g., send output to file instead of terminal
       - ~ls -alR > ls-alR.txt~
	 - Process for ~ls -alR~ generates (recursive) directory listing
	 - The ~>~ operator *redirects* ~stdout~ of process to file
           ~ls-alR.txt~
	 - File overwritten if existing, else newly created
   #+ATTR_REVEAL: :frag appear
   - Streams can be *connected via pipes*
     - E.g., send ~stdout~ of one process to ~stdin~ of another
       - ~ls -alR | wc -l~
	 - Here, ~stdout~ of process for ~ls~ is connected via
            *pipe operator* (~|~) to ~stdin~ of process for ~wc~
	 - (~wc~ counts words of its input, with option ~-l~ it counts
           lines; ~ls~ with option ~-l~ lists one file/directory per line)

** JiTT Assignments
   :PROPERTIES:
   :reveal_data_state: jitt no-toc-progress
   :END:

*** Bash Tutorial
    :PROPERTIES:
    :CUSTOM_ID: bash-tutorial
    :reveal_data_state: jitt no-toc-progress
    :END:
   - Processes and GNU/Linux command line
     - Command line implemented by a process called “shell”
       - Lots of shell variants;
         [[https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29][Bash]] used here
     - (Shell and GUI are not part of the OS; each provides another
       layer of abstraction and interface)
     - (There is something called [[https://msdn.microsoft.com/en-us/commandline/wsl/about][Bash on Windows]],
       about which I read that it exists, nothing more)
     - (On Macs, Bash is the default shell for terminal windows;
       students reported that most parts of the tutorial work for them)
   - Execution of commands (programs) in shell creates new processes
     - Typical command creates one single-threaded process
   - Web tutorial http://ryanstutorials.net/linuxtutorial/
   - Your task: Work through sections 1, 2, 3, 4, 5, 11, 12
     - Read Learnweb announcement for GNU/Linux access

*** JiTT Assignment
    :PROPERTIES:
    :reveal_data_state: jitt no-toc-progress
    :END:
    Explore file descriptors and mark correct
    statements in [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=840261][Learnweb]].

    - ~/proc/<pid>/fd~: Sub-directory of [[file:OS08-Memory.org::#proc][/proc]] with file descriptors
    - Start
      1. ~less /etc/passwd~
      2. ~less < /etc/passwd~
      3. ~cat /etc/passwd | less~
    - All show contents of file ~/etc/passwd~
      - Via process that executes ~less~
      - Use ~ps -C less~ to identify process IDs
      - Use ~ls -l /proc/<pid>/fd~ to see file descriptors
      - Options
	- Command (1) creates fewer file descriptors than command (2).
	- Command (1) accesses the file via stdin.
	- Command (2) accesses the file via stdin.
	- Command (3) accesses the file via stdin.
	- Command (1) accesses the file via the file descriptor numbered 0.
	- Command (2) accesses the file via the file descriptor numbered 0.
	- Command (3) accesses the file via the file descriptor numbered 0.
	- Command (3) creates two processes, while (1) and (2) each
          create one process.
	- By creating an unnecessary process for ~cat~, command (3)
          involves unnecessary overhead.
	- Command (3) can be simplified to ~/etc/passwd > less~
	- In command (3), stdin of ~cat~ is redirected to stdin of ~less~
	- Commands of the form ~cat <file> | X~ follow an
          anti-pattern as they can be simplified to ~X < <file>~

* Access Rights

** Fundamentals of Access Rights
   :PROPERTIES:
   :CUSTOM_ID: acess-rights
   :END:
   #+ATTR_REVEAL: :frag (appear)
   - Who is allowed to do what?
   - System controls access to *objects* by *subjects*
     - Object = whatever needs protection: e.g., region of memory,
       file, service
       - With different *operations* depending on type of object
     - Subject = active entity using objects: process
       - Threads of process *share* same access rights
       - Subject may also be object, e.g., terminate thread or process
   - Subject acts on behalf of *principal*
     - Principal = User or organizational unit
     - Different principals and subjects have different *access rights*
       on different objects
       - Permissible operations

*** Typical Access Right Operations
    - In general, dependent on object type, e.g.:
      - Files
	- Create, destroy
	- Read, write, append
	- Execute
	- Ownership
      - Access rights
	- Copy/grant

** Representation of Access Rights
   - Conceptual: [[#access-matrix][Access (control) matrix]]
   - Slices of access matrix
     - [[#capabilities][Capabilities]]
     - [[#acl][Access control lists]]

*** Access (Control) Matrix
   :PROPERTIES:
   :CUSTOM_ID: access-matrix
   :END:
    - Matrix
      - Principals and subjects as rows
      - Objects as columns
      - List of permitted operations in cell

*** Access Matrix: Transfer of Rights
   :PROPERTIES:
   :CUSTOM_ID: access-matrix
   :reveal_extra_attr: data-audio-src="./audio/9-access-matrix-a.ogg"
   :END:
    - Transfer of rights from principal JDoe to process P_1
      - Figure 7.12 (a) of cite:Hai17: copy rights
	|      | F_1  | F_2   | JDoe | P_1 | … |
	|------+------+-------+------+-----+---|
	| JDoe | read | write |      |     |   |
	| P_1  | read | write |      |     |   |
	| ⋮    |      |       |      |     |   |
	|------+------+-------+------+-----+---|
      #+ATTR_REVEAL: :frag appear :audio ./audio/9-access-matrix-b.ogg
      - Figure 7.12 (b) of cite:Hai17: special right for transfer of rights
        |      | F_1  | F_2   | JDoe          | P_1 | … |
        |------+------+-------+---------------+-----+---|
        | JDoe | read | write |               |     |   |
        | P_1  |      |       | use rights of |     |   |
        | ⋮    |      |       |               |     |   |
        |------+------+-------+---------------+-----+---|
#+BEGIN_NOTES
This small excerpt of an access matrix demonstrates
(1) the representation of access rights in general as well as
(2) the transfer of access rights under the variants
    (a) by copying and
    (b) with a special operation.

1. Representation of access rights.
   In the columns, different objects are shown, namely two files
   called F_1 and F_2, principal JDoe, and process P_1.
   Note that JDoe and P_1 occur in column headers as well as row
   headers, indicating that they serve dual roles as objects and subjects.
   Access right of process P_1 (as subject) are indicated in the row
   for P_1.  You see that P_1 is allowed to read file F_1 and write
   file F_2.  You also see that subjects JDoe and P_1 share the same
   access rights.

2. Transfer of access rights.
   Processes obtain their access rights from principals
   (users) on whose behalf they are operating.  For example, if you
   and me have got user accounts on my machine and if both of us start
   the same text editor, then the two processes for these text editors
   will have different access rights, which are derived from our
   (users’) access rights: Typically, you will be able to read and write
   your own files, while you should be unable to access my files
   (say, the final exam for this course), and vice versa.

   In this example, P_1 is a process working on behalf of principal
   (user) JDoe.

2.a In this first variant of the access matrix, the rights of JDoe
    were simply copied to P_1 when P_1 was created by JDoe.

2.b A second variant for the transfer of access rights might be used,
    which avoids copying lots of access rights.  Towards that end, a
    special operation may be used in the access matrix, which treats
    principals as objects.  Here, you see that process P_1 has the
    right to “use rights of” JDoe.  Consequently, when P_1 tries to
    access some object, the OS will check JDoe’s rights.
#+END_NOTES

*** Capabilities
   :PROPERTIES:
   :CUSTOM_ID: capabilities
   :END:
    - *Capability* ≈ reference to object with access rights
    - Conceptually, capabilities arise by slicing the access
      matrix row-wise
      - Principals have lists with capabilities (access rights) for objects
      - Challenge: Tampering, theft, revocation
	- Capabilities may contain cryptographic authentication codes

*** Access Control Lists
   :PROPERTIES:
   :CUSTOM_ID: acl
   :END:
    - *Access Control List* (*ACL*) = List of access rights for
      subjects/principals attached to object
    - Conceptually, ACLs arise by slicing the access matrix column-wise
      - E.g., [[#ex-acls][file access rights in GNU/Linux]] and Windows
	(see Sec. 7.4.3 in cite:Hai17)

** Access Control Paradigms
   - Discretionary access control (*DAC*)
     - *Owner* grants privileges
     - E.g., file systems
   - Mandatory access control (*MAC*)
     - *Rules* about properties of principals, processes, resources define
       permitted operations
   - Role based access control (*RBAC*)
     - Permissions for tasks bound to organizational roles

*** DAC vs MAC
    - With DAC, *users* are in control
      - Users are lazy
      - If defaults are too restrictive, too permissive rights may be granted
	- “Allow all” is simpler than fine-grained control
    - With MAC, a *system* of rules is in control
      - E.g., [[https://en.wikipedia.org/wiki/Security-Enhanced_Linux][SELinux]],
	[[https://en.wikipedia.org/wiki/AppArmor][AppArmor]]
      - More complex to manage/use
      - Respects more
        [[file:OS10-Security.org::#design-principles][design principles for secure systems]]

** JiTT Assignment
   :PROPERTIES:
   :reveal_data_state: jitt no-toc-progress
   :END:
   Answer the following question in [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=840261][Learnweb]]
   or Etherpad (see [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/forum/view.php?id=802190][Discussion Forum]] in Learnweb).

   {{{understandingquestion}}}


* In-Class Meeting

** ~/proc~ Revisited
   :PROPERTIES:
   :CUSTOM_ID: proc-status
   :END:
   - [[file:OS08-Memory.org::#proc][Recall]]: ~/proc~ is a pseudo-filesystem
     - Process listing command ~ps~ inspects ~/proc~
       - E.g., ~ps -e~ shows some details on all processes (IDs, time, etc.)
   - ~/proc/<pid>/status~
     - File with status information of process
       - View with, e.g.: ~cat /proc/42/status~
   - Selected information
     - Process ID (also of parent process)
     - Information concerning memory usage
     - ~voluntary_ctxt_switches~
       - Process gave up CPU (yield) or did system call
     - ~nonvoluntary_ctxt_switches~
       - Process removed from CPU (preempted) by OS

** Quiz
   - Consider two infinite loops
     1. ~while true; do true; done~
	- Command ~true~ immediately returns successfully
     2. ~while true; do sleep 1; done~
	- Command ~sleep~ causes process to sleep for indicated number
          of seconds
   - For which loop do you expect what context switch counter(s) to
     increase (dominantly)?


#+MACRO: blackcode @@html:<code><span style="color:black; font-size:smaller;">$1</span></code>@@ @@latex:\texttt{$1}@@
#+MACRO: redcode @@html:<code><span style="color:darkred; font-size:smaller;">$1</span></code>@@ @@latex:\rverb|$1|@@
#+MACRO: greencode @@html:<code><span style="color:darkgreen; font-size:smaller;">$1</span></code>@@ @@latex:\gverb|$1|@@
#+MACRO: bluecode @@html:<code><span style="color:darkblue; font-size:smaller;">$1</span></code>@@ @@latex:\bverb|$1|@@

** File ACLs in GNU/Linux (1/2)
   :PROPERTIES:
   :CUSTOM_ID: ex-acls
   :END:
   - {{{blackcode(ls -l /etc/shadow /usr/bin/passwd)}}}
     - {{{blackcode(-)}}}{{{redcode(rw-)}}}{{{bluecode(r--)}}}{{{greencode(---)}}}{{{blackcode( 1 )}}}{{{redcode(root)}}}{{{bluecode( shadow  )}}}{{{blackcode(1465 Jan 21  2015 /etc/shadow)}}}
     - {{{blackcode(-)}}}{{{redcode(rws)}}}{{{bluecode(r-x)}}}{{{greencode(r-x)}}}{{{blackcode( 1 )}}}{{{redcode(root)}}}{{{bluecode( root   )}}}{{{blackcode(47032 Jan 27 01:40 /usr/bin/passwd*)}}}
   - {{{blackcode(ls -ld /tmp)}}}
     - {{{blackcode(d)}}}{{{redcode(rwx)}}}{{{bluecode(rwx)}}}{{{greencode(rwx)}}}{{{blackcode( 14 )}}}{{{redcode(root)}}}{{{bluecode( root  )}}}{{{blackcode(20480 Jul  4 13:20 /tmp)}}}
       - File type and permissions
	 - File (~-~), directory (~d~), symbolic link (~l~), …
	 - Read (~r~), write (~w~), execute (~x~)
	 - Set user/group ID (~s~), sticky bit (~t~)
       - Shortened ACLs
	 - Permissions not for individual users
	 - Instead, separately for [[color:darkred][owner]],
	   [[color:darkblue][group]], [[color:darkgreen][other]]
	 - [[color:darkred][Owner]]: Initially, the creator; ownership can be transferred
	 - [[color:darkblue][Group]]: Users can be grouped, e.g., to share files for a joint project
	 - [[color:darkgreen][Other]]: Everybody else

** File ACLs in GNU/Linux (2/2)
   - Management of ACLs via ~chmod~, ~chown~, ~umask~
   - Suggested task: Continue [[#bash-tutorial][Bash tutorial]]
     - Work through section “8. Permissions”

* Conclusions

** Summary
   - Process as unit of management and protection
     - Threads with address space and resources
       - Including file descriptors
     - Access control as one protection mechanism
   - User vs kernel threads

** Learning Objectives
   - Explain process and thread concept
   - Discuss state transitions for threads under different thread implementations
   - Perform simple tasks in Bash
     - View directories and files, inspect files under ~/proc~, build
       pipelines, redirect in- or output, list processes with ~ps~
   - Explain access control, access matrix, and ACLs

bibliographystyle:alphadin
bibliography:csos.bib

#+MACRO: copyrightyears 2017
#+INCLUDE: license-template.org

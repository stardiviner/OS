# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: config.org

#+TITLE: OS07: MX Challenges
#+SUBTITLE: Based on Chapter 4 of cite:Hai17
#+AUTHOR: Dr. Jens Lechtenbörger
#+DATE: Computer Structures and Operating Systems 2017

* Introduction

# This is the seventh presentation for this course.
#+CALL: generate-plan(number=7)

#+TOC: headlines 1

** Recall
   - Mutexes may be based either on busy waiting ([[file:OS05-MX.org::#spinlocks][spinlocks]])
     or on blocking (e.g., [[file:OS05-MX.org::#monitor-origin][monitor]],
     [[file:OS05-MX.org::#semaphore-origin][semaphore]])
   - Threads may have different *priorities*
     - Lower priority threads are *preempted* for those with higher
       priority (e.g., with [[file:OS04-Scheduling.org::#roundrobin][round robin scheduling]])

*** Different Learning Styles
    - In 2017, some students reported that Section 4.8.1 (pp. 135
      -- 137) of cite:Hai17 on Priority Inversion is quite easy to
      understand, while they perceived
      [[#priority-inversion][that section in this presentation]]
      to be confusing
    - Note that cite:Hai17 discusses Priority Inversion resulting from
      locks/mutexes with blocking, while the slides also contain a
      variant with spinlocks.

* Priority Inversion
  :PROPERTIES:
  :CUSTOM_ID: priority-inversion
  :reveal_extra_attr: data-audio-src="./audio/7-pi.ogg"
  :END:
#+BEGIN_NOTES
In general, if threads with different priorities exist, the OS should
run those with high priority in preference to those with lower
priority.

The technical term “priority inversion” denotes phenomena, where
low-priority threads hinder the progress of high-priority threads,
which intuitively should not happen.
The next slides demonstrate such phenomena, first a weaker variant
where MX is enforced with spinlocks, then the more usual variant with
MX based on blocking.
#+END_NOTES

#+MACRO: T0   [[color:darkgreen][T_0]]
#+MACRO: TM   [[color:darkorange][T_M]]
#+MACRO: T1   [[color:red][T_1]]

** Priority Inversion with Spinlocks
   :PROPERTIES:
   :CUSTOM_ID: pi-spinlocks
   :END:
   - Example; single CPU core
     - Thread {{{T0}}} with low priority enters CS
   - {{{T0}}} preempted by OS for {{{T1}}} with high priority
   - {{{T1}}} tries to enter same CS and *spins* on lock held by {{{T0}}}

   #+ATTR_REVEAL: :frag appear
   - This is a variant of *priority inversion*
     - High-priority thread {{{T1}}} cannot continue due to actions by
       low-priority thread
     - If just one CPU core exists: Low-priority thread {{{T0}}}
       cannot continue (as CPU occupied by {{{T1}}})
       - Deadlock (discussed [[#deadlock][subsequently]])
     - If multiple cores exist: Low-priority thread {{{T0}}} runs
       although thread with higher priority does not make progress

** Priority Inversion with Blocking
   :PROPERTIES:
   :CUSTOM_ID: pi-blocking
   :END:
   - Consider {{{T0}}} with [[color:darkgreen][low]], {{{TM}}} with [[color:darkorange][medium]],
     {{{T1}}} with [[color:red][high]] priority
     - {{{T0}}} in CS
     - OS preempts {{{T0}}} for {{{T1}}}
       - {{{T1}}} attempts entry into same CS, {{{T1}}} gets blocked
       - {{{T0}}} could continue if no higher priority thread existed
   #+ATTR_REVEAL: :frag appear
   - OS favors {{{TM}}} over {{{T0}}}
     - {{{TM}}} runs instead of more highly prioritized {{{T1}}} → *Priority
       inversion*
     - {{{T0}}} cannot leave CS as long as {{{TM}}} exists
   #+ATTR_REVEAL: :frag appear
   - With long or many threads of [[color:darkorange][medium]] priority,
     {{{T1}}} needs to wait for a long time

** Priority Inversion Example
   - Mars Pathfinder, 1997; see
     [[https://en.wikipedia.org/wiki/Mars_Pathfinder][Wikipedia]]
     - Robotic spacecraft named Pathfinder
       - With rover named Sojourner (see picture from [[https://en.wikipedia.org/wiki/File:Sojourner_on_Mars_PIA01122.jpg][wikimedia.org]], created by NASA, public domain)
         [[file:img/262px-Sojourner_on_Mars_PIA01122.jpg]]
     - A “low-cost” mission at $280 million
   - Bug caused repeated resets
     - “found in preflight testing but was deemed a *glitch* and therefore
       given a low priority as it only occurred in certain *unanticipated*
       heavy-load conditions”
   - Priority inversion had been known for a long time
     - E.g.: cite:LR80

** Priority Inversion Solutions
   :PROPERTIES:
   :CUSTOM_ID: priority-inheritance
   :reveal_extra_attr: data-audio-src="./audio/7-priority-inheritance-1.ogg"
   :END:
   - *Priority Inheritance (PI)*
     - Thread of low priority inherits priority of waiting thread
       - E.g., [[file:OS05-MX.org::#pi-futex][PI-futex in Linux]]
       - E.g., [[https://www.microsoft.com/en-us/research/people/mbj/#just-for-fun][remote update for Mars Pathfinder]]
	 - Mutex of Pathfinder OS had flag to activate PI
	 - Initially, PI was off …
   #+ATTR_REVEAL: :frag appear :audio ./audio/7-priority-ceiling.ogg
   - *Priority Ceiling (PC)*
     - Every resource has priority (new concept; so far only threads
       had priorities)
       - (Highest priority that “normal” threads can have) + 1
     - Accessing thread runs with that priority
   #+ATTR_REVEAL: :frag appear :audio ./audio/7-priority-inheritance-2.ogg
   - In both cases: Restore old priority after access
#+BEGIN_NOTES
An intuitive explanation of Priority Inheritance is that if an
important task, i.e., a thread with high priority, needs to wait for
“something else”, then this “something else” immediately gains in
importance, as the success of the important task depends on it.

Thus, with Priority Inheritance a thread of low priority holding some
lock, mutex, semaphore, or monitor, for which a high priority threads waits,
inherits the priority of the high priority waiting thread.

If you think again about the negative effects of medium priority
threads for priority inversion, those negative effects do no longer
occur, as the thread with inherited priority is now scheduled before
medium priority threads.  Thus, it finishes fast, allowing the high
priority thread to continue quickly.

For Priority Ceiling, each resource is assigned a priority.  The
priority of a thread accessing that resource is then increased to the
resource’s priority.  Usually, the priority for resources is set to be
slightly higher than that of ordinary threads.  Thus, for the duration
of resource accesses, threads run with highest priority and finish
quickly.

In both cases, Priority Inheritance and Priority Ceiling, priorities
are restored after resource access.

To conclude, you should think twice whether you want to create threads
with different priorities that share resources.  If yes, priority
inversion may happen.  Then, you need to check the documentation for
whatever MX mechanism you are about to apply whether it supports PI or
PC.  If neither is documented, do not use that mechanism.
#+END_NOTES

** JiTT Assignment
   :PROPERTIES:
   :reveal_data_state: jitt no-toc-progress
   :END:

   Solve Exercise 4.10 (on priority inversion, assuming that blocking
   mutexes are used) of cite:Hai17 and submit your solution in
   [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=840263][Learnweb]].


* Deadlocks

** Deadlock
   :PROPERTIES:
   :CUSTOM_ID: deadlock
   :END:
   - Permanent blocking of thread set
     - Reason
       - *Cyclic waiting* for resources/locks/messages of other threads
   - No generally accepted solution
     - Deadlocks can be perceived as programming bugs
       - Dealing with deadlocks causes overhead
	 - Acceptable to deal with (hopefully rare) bugs?
     - Solutions depend on
       - Properties of resources
       - Properties of threads (transactions?)

** Deadlock Example
   :PROPERTIES:
   :CUSTOM_ID: deadlock-ex
   :END:
   - Money transfers between bank accounts
     - Transfer from Account#1 to Account#2 by thread 1; transfer in
       other direction by thread 2
   #+ATTR_REVEAL: :frag appear
   - *Race conditions* on account balances
   - Need *mutex* per account
     - Lock both accounts involved in transfer. What order?
   #+ATTR_REVEAL: :frag appear
   - “Natural” lock order: First, lock source account; then, lock
     destination account
     - Thread 1 locks Account#1, while thread 2 locks Account#2
       - Each thread gets blocked once it attempts to acquire the
         second lock
	 - Neither can continue
       - *Deadlock*

** Defining Conditions for Deadlocks
   :PROPERTIES:
   :CUSTOM_ID: deadlock-def
   :END:
   Deadlock if and only if (1) -- (4) hold cite:CES71:

   1. *Mutual exclusion*
      - Exclusive resource usage
   2. *Hold and wait*
      - Threads hold some resources while waiting for others
   3. *No preemption*
      - OS does not forcibly remove allocated resources
   4. *Circular wait*
      - Circular chain of threads such that each thread holds
        resources that are requested by next thread in chain

** Resource Allocation Graphs
   - Visualization of resource allocation as *directed graph*
     - Nodes
       - Threads (squares on next slide)
       - Resources (circles on next slide)
     - Edges
       - From thread T to resource R if T is waiting for R
       - From resource R to thread T if R is allocated to T
   - *Fact*: System in deadlock if and only if graph contains cycle

** Resource Allocated Graph Example
   Visualization of deadlock: cyclic resource allocation graph
   for [[#deadlock-ex][previous example]]

   #+CAPTION: Figure 4.22 of cite:Hai17
   #+ATTR_HTML: :alt Resource Allocated Graph for Accounts
   [[./img/hail_f0422.pdf.png]]


* Deadlock Strategies

** Deadlock Strategies
   - (Ostrich algorithm)
   - Deadlock Prevention
   - Deadlock Avoidance
   - Deadlock Detection

   These strategies are covered in subsequent slides.

** Ostrich “Algorithm”
   :PROPERTIES:
   :CUSTOM_ID: ostrich
   :END:
   #+BEGIN_leftcol
   - “Implemented” in most systems
     - Pretend nothing special is happening
     - (E.g., Java VMs act like ostriches)
   - Reasoning
     - Proper deadlock handling is complex
     - Deadlocks are rare, result from buggy programs
   #+END_leftcol

   #+BEGIN_rightcol
   #+HTML: <div class="figure randomPic"><!-- { "imgalt": "Ostrich visualization", "imgcaption": "<p>Drawing created by {name} as bonus task in summer term 2017; randomly selected from <a href='https://gitlab.com/oer/OS/tree/master/img/ostriches'>excellent drawings</a> (refresh for others)</p>", "choices": [ {"name": "Thomas Ackermann", "path": "./img/ostriches/ostrich-ackermann.png"}, {"name": "Fabian Buschmann", "path": "./img/ostriches/ostrich-buschmann.png"}, {"name": "Adrian Lison", "path": "./img/ostriches/ostrich-lison.png"}, {"name": "Felix Murrenhoff", "path": "./img/ostriches/ostrich-murrenhoff.png"}]} --></div>
   #+END_rightcol
   # Normally, this would be enough for a single image:
   # #+BEGIN_rightcol
   # #+CAPTION: Drawing created by <name> as bonus task in summer term 2017; [[https://gitlab.com/oer/OS/tree/master/img/ostriches][other excellent drawings]]
   # #+ATTR_HTML: :alt Ostrich visualization
   # [[./path/to/<img.png>]]
   # #+END_rightcol

#+BEGIN_EXPORT latex
\begin{figure}[htp]
\centering
\includegraphics[width=.9\linewidth]{img/ostriches/ostrich-lison.png}
\caption{Drawing created by Adrian Lison as bonus task in summer term 2017; \href{https://gitlab.com/oer/OS/tree/master/img/ostriches}{other excellent drawings}.}
\end{figure}
#+END_EXPORT

** Deadlock Prevention
  :PROPERTIES:
  :CUSTOM_ID: deadlock-prevention
  :reveal_extra_attr: data-audio-src="./audio/7-prevention.ogg"
  :END:
   - Prevent a [[#deadlock-def][defining condition]] for deadlocks
     from becoming true
   - Practical options
     #+ATTR_REVEAL: :frag appear
     - Prevent condition (2), “hold and wait”: Request all necessary
       resources at once
       - Only possible in special cases, e.g., static 2PL in DBMS
       - Threads either have no incoming or no outgoing edges in
         resource allocation graph → Cycles cannot occur
     #+ATTR_REVEAL: :frag appear
     - Prevent condition (4), “circular wait”: Number resources,
       request resources according to linear resource ordering
       - Requests for resources in ascending order → Cycles cannot occur
       - In other words: A thread cannot request a resource R_k if it
         is holding a resource R_h with k < h
#+BEGIN_NOTES
A strategy for deadlocks is called prevention strategy if it prevents
deadlocks from happening by making sure that one of the four defining
deadlock conditions can never become true.  Although there are
four conditions, only two of them are used for practical purposes, and
they are explained in the subsequent bullet points.
Please think about those bullet points on your own.
#+END_NOTES

*** Linear Resource Ordering Example
    - Money transfers between bank accounts revisited
    - Locks acquired in *order of account numbers*
      - A programming contract, not known by OS
      - Both threads try to lock Account#1 first
	- Only one succeeds, can also lock Account#2
	- The other thread gets blocked
      - *No deadlock*
    - (See Fig 4.21 in cite:Hai17 for an example of linear ordering in
      the context of the Linux scheduler)

** Deadlock Avoidance
  :PROPERTIES:
  :CUSTOM_ID: deadlock-avoidance
  :reveal_extra_attr: data-audio-src="./audio/7-avoidance-1.ogg"
  :END:
   - (See [[https://ell.stackexchange.com/questions/52710/the-difference-between-prevent-and-avoid][stackexchange for difference between prevent and avoid]])
#+ATTR_REVEAL: :frag (appear) :audio (./audio/7-avoidance-2.ogg ./audio/7-avoidance-3.ogg)
   - Dynamic decision whether allocation may lead to deadlock
     - If a deadlock cannot be ruled out easily: Do not perform that
       allocation but *block* the requesting thread (or return error
       code or raise exception)
     - Consequently, deadlocks do never occur; they are *avoided*
   - Classical technique
     - [[https://en.wikipedia.org/wiki/Banker's_algorithm][Banker’s algorithm]] by Dijkstra
       - Deny incremental allocation if “unsafe” state would arise
       - Not used in practice
	 - Resources and threads’ requirements need to be declared ahead of
           time
#+BEGIN_NOTES
A strategy for deadlocks is called avoidance strategy if it avoids
deadlocks.  Personally, I don’t see much difference between the words
“prevent” and “avoid”, but this terminology is accepted in the
literature on deadlocks.

Avoidance does not rule out any specific of the four defining deadlock
conditions, but it still makes sure that deadlocks will not happen.
The typical approach is to analyze resource requests by threads.  If
some deadlock avoidance algorithm is able to rule out a deadlock for
the resulting state, the request will be granted.  If the algorithm is
not able to rule out deadlocks, the request will not be granted.  Note
that such algorithms generally err on the safe side.  Thus, some
requests might not be granted although they would not cause any
deadlock; the OS might be unable to detect this, though.

A famous deadlock avoidance technique is Dijkstra’s banker’s
algorithm, which has quite restrictive preconditions and is therefore not
used in practice.
#+END_NOTES

** Deadlock Detection
  :PROPERTIES:
  :CUSTOM_ID: deadlock-detection
  :reveal_extra_attr: data-audio-src="./audio/7-detection-1.ogg"
  :END:
   - Idea
     - Let deadlocks happen
     - Detect deadlocks, e.g., via cycle-check on resource allocation graph
       - Periodically or
       - After “unreasonably long” waiting time for lock or
       - Immediately when thread tries to acquire a locked mutex
     - Resolve deadlocks: typically, terminate some thread(s)
   #+ATTR_REVEAL: :frag appear :audio ./audio/7-detection-2.ogg
   - Prerequisite to build graph
     - Mutex records by which thread it is locked (if any)
     - OS records for what mutex a thread is waiting
#+BEGIN_NOTES
The final strategy for dealing with deadlocks is deadlock detection.
Here, the system does not take special precautions to avoid or prevent
deadlocks but lets them happen.  To deal with deadlocks, they are
detected, for example based on cycle checks on resource allocation
graphs, and then resolved.  Detection may take place periodically or after
waiting times or even immediately upon resource requests; the latter
actually prevents cyclic wait conditions, moving from deadlock
detection to deadlock prevention.
To resolve deadlocks, the OS typically terminates some threads until
no cycle exists any longer, and various strategies exist to select
victim threads.

Clearly, the OS needs to build suitable data structures for deadlock
detection, in case of resource allocation graphs, each mutex can
easily record by which thread it is locked, while the OS also keeps
track of what threads are waiting for what mutexes.
#+END_NOTES

** JiTT Assignments
   :PROPERTIES:
   :reveal_data_state: jitt no-toc-progress
   :END:

*** General JiTT Survey
    :PROPERTIES:
    :reveal_data_state: jitt no-toc-progress
    :END:

    Please take about 5 to 10 minutes to answer this
    [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/feedback/view.php?id=842705][anonymous survey in Learnweb]]
    on your thoughts concerning the switch of Operating Systems to
    Just-in-Time Teaching (JiTT) and the format of presentations.

*** MX Challenges
    :PROPERTIES:
    :reveal_data_state: jitt no-toc-progress
    :END:

    Answer the following
    [[http://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=840263][questions in Learnweb]].

    1. Extend the [[#deadlock-ex][deadlock example]] concerning bank
       accounts in such a way that five accounts are involved in a
       deadlock.  Draw the corresponding resource allocation graph and
       explain how the cycle would be avoided if accounts were locked
       according to a linear ordering.

    2. {{{understandingquestion}}}


* In-Class Meeting

** Convoy Problem
   :PROPERTIES:
   :CUSTOM_ID: convoy
   :END:
   #+ATTR_REVEAL: :frag (appear)
   - Suppose a central shared resource exists
     - *Frequently accessed* by lots of threads
     - Protected by *mutex M*
   - Preemption of thread T1 holding that mutex is likely
     - Other threads wind up in wait queue of mutex, the *convoy*
       - Thread switches *without much progress*
   - Suppose T1 continues
     - T1 releases lock, which is reassigned to T2
     - During its time slice, T1 wants M again, which is now held by T2
       - T1 gets blocked *without much progress*
   - The same happens to the other threads
     - The convoy *persists* for a long time

*** Convoy Solution
    :PROPERTIES:
    :CUSTOM_ID: convoy-solution
    :END:
    - Change [[file:OS05-MX.org::#mutex-queue][mutex]] behavior
      - Do no immediately reassign mutex upon ~unlock()~
      - Instead, make *all* waiting threads runnable
	- Without reassigning mutex
    - Effect: T1 can ~lock()~ M repeatedly during its time slice

** Starvation
   :PROPERTIES:
   :CUSTOM_ID: starvation
   :END:
   - A thread *starves* if its resource requests are repeatedly denied
   - Examples
     - [[file:OS04-Scheduling.org::#priority-starvation][Thread with low priority in presence of high priority threads]]
     - [[file:OS05-MX.org::#mx-challenges][Thread which cannot enter CS]]
       - Famous illustration: Dining philosophers (next slide)
       - No simple solutions

*** Dining Philosphers
    - MX problem proposed by Dijkstra
    - Philosophers sit in circle; *eat* and think repeatedly
      - Two *forks* required for eating
	- *MX* for forks

   #+CAPTION: Figure 4.20 of cite:Hai17
   #+ATTR_HTML: :alt Dining Philosophers
   [[./img/hail_f0420.60.png]]

*** Starving Philosophers
    Starvation of P0
    [[./img/hail_f0420.60.png]]
    - P1 and P3 or P2 and P4 eat in parallel
    - Then they wake the other pair
      - P1 wakes P2; P3 wakes P4
      - P2 wakes P1; P4 wakes P3
    - Iterate


* Conclusions

** Summary
   - MX to avoid race conditions
   - Challenges
     - Priority inversion
     - Deadlocks
     - Convoys
     - Starvation

** Learning Objectives
   - Explain priority inversion and counter measures
   - Explain and apply deadlock prevention and detection
   - Explain convoys and starvation as MX challenges

bibliographystyle:alphadin
bibliography:csos.bib

#+MACRO: copyrightyears 2017
#+INCLUDE: license-template.org
